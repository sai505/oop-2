// (c) Andrew Sutton, PhD
// All rights reserved

#pragma once

// Nested class?
struct Options
{
  int num_decks = 1;
  bool ace_high = true;
  int num_sacrifice = 1;
  bool negotiable_sacrifice = true;
};


struct Game
{
  void step();
  void run();

  Options options;
  Deck deck;
  Player p1;
  Player p2;
  Pile pile;
  int turn;
};


#pragma once
#include "card.h"
#include "iostream"
#include <vector>

using Pile = std::vector<Card>;


class Player {
public:
	//split the deck to hand
	Player(Pile&, int);

	//Playing hand
	//Card& playing_hand(Pile&, int);

	//Draw hand
	//Print
	void Print() const;

private:
	Pile hand;
	Pile playing;

};
// (c) Andrew Sutton, PhD
// All rights reserved

#include <utility>

#include <iosfwd>

// Ranks of a card are:
// ace, 2-10, jack, king, and queen.

// Suits of a card are:
// hearts, diamonds, clubs, spades

// using Card = int[2];
// using Card = std::pair<int, int>;

enum Rank // An enumeration type
{
  Ace, // An enumerator
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};

enum Suit
{
  Hearts,
  Diamonds,
  Clubs,
  Spades,
};

// A playing card (in a standard deck) is a pair of rank and
// suit (see enums above).
class Card
{
public:
  // // Maybe not great. Creates a "not a card" abstraction.
  // Card()
  //   : rank(), suit()
  // { }

  // Possibly better. Creates an uninitialized value.
  // Card() { }

  // Good. Make the compiler do the right thing.
  Card() = default;

  // Construct a card with a rank and suit.
  Card(Rank r, Suit s)
    : rank(r), suit(s) // member initializer list
  { }


  // // Copy constructor.
  // Card(const Card& c)
  //   : rank(c.rank), (c.suit)
  // { }

  // // Copy assignment operator.
  // Card& operator=(const Card& c)
  // {
  //   rank = c.rank;
  //   suit = c.suit;
  //   return *this;
  // }


  // Accessor functions.
  // Observer functions.

  // Returns the rank of the card.
  Rank get_rank() const { return rank; }

  // Returns the suit of the card.
  Suit get_suit() const { return suit; }

  // // Mutator functions.
  // // Modifier functions.
  //
  // Don't provide mutators if they're just assignment.
  // Prefer to make things public (unless your boss says
  // otherwise).
  //
  // void set_rank(Rank r) { rank = r; }
  // void set_suit(Suit s) { suit = s; }

private:
  Rank rank;
  Suit suit;
};

// Equality comparison
bool operator==(Card a, Card b);
bool operator!=(Card a, Card b);

// Ordering
bool operator<(Card a, Card b);
bool operator>(Card a, Card b);
bool operator<=(Card a, Card b);
bool operator>=(Card a, Card b);

std::ostream& operator<<(std::ostream& os, Card c);
std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Suit s);


// /// A member function is kind of like a normal function.
// /// It is (roughly) equivalent to this:
// ///
// /// NOTE: Not valid C++ (because this is reserved word).
// Rank get_rank(const Card* this) {
//   return this->rank;
// }




